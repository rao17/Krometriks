package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

//Activity to display the result of the sample analysis and store the result in a file

public class DisplayResult extends Activity implements View.OnClickListener {

    TextView ResultStatement, Result;
    EditText SampleId;
    Button BackMainMenu, RedoSample, SaveResult;

    double resultVal = 0;
    String units, curveName="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_result);

        ResultStatement = (TextView) findViewById(R.id.ResultStatement);
        Result = (TextView) findViewById(R.id.Result);
        SampleId = (EditText) findViewById(R.id.SampleId);

        SaveResult = (Button) findViewById(R.id.SaveResult);
        SaveResult.setOnClickListener(this);
        SaveResult.setBackgroundColor(Color.rgb(171, 217, 233));

        RedoSample = (Button) findViewById(R.id.RedoSample);
        RedoSample.setOnClickListener(this);
        RedoSample.setBackgroundColor(Color.rgb(253, 174, 97));

        BackMainMenu = (Button) findViewById(R.id.BackMainMenu);
        BackMainMenu.setOnClickListener(this);
        BackMainMenu.setBackgroundColor(Color.rgb(211, 211, 211));

        SharedPreferences pref = getApplicationContext().getSharedPreferences("Pref", MODE_PRIVATE);
        int n = pref.getInt("CurveNumber", 1);
        curveName = getCurve(n);

        //Get the calculated result from
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            resultVal = extras.getDouble("Result");
        }

        units = getUnits(resultVal);
        resultVal = roundingDecimalPlaces(resultVal, 3, units);
        show_val();


    }

    //Get the appropriate units of the result
    public String getUnits(double num) {
        String unit = "";
        if (num * Math.pow(10, -6) > 1)
            unit = "M";
        else if (num * Math.pow(10, -6) < 1 && num * Math.pow(10, -3) > 1)
            unit = "mM";
        else if (num * Math.pow(10, -3) < 1 && num * Math.pow(10, 0) > 1)
            unit = "\u00B5M";
        else if (num * Math.pow(10, 0) < 1 && num * Math.pow(10, 3) > 1)
            unit = "nM";
        else if (num * Math.pow(10, 3) < 1)
            unit = "pM";
        return unit;
    }

    //Round off the numerical value of the result corresponding to the appropriate unit
    public double roundingDecimalPlaces(double num, int n, String unit) {
        int i = 0;
        switch (unit) {
            case "M":
                i = -6;
                break;
            case "mM":
                i = -3;
                break;
            case "\u00B5M":
                i = 0;
                break;
            case "nM":
                i = 3;
                break;
            case "pM":
                i = 6;
                break;
            default:
                break;
        }

        num = num * Math.pow(10, i);

        double new_num = num * Math.pow(10, n);
        new_num = Math.round(new_num);
        return (new_num / Math.pow(10, n));

    }


    //Show result
    public void show_val() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                ResultStatement.setText("The concentration is");
                Result.setText(String.valueOf(resultVal) + " " + units);
            }
        }, 1000);
    }

    //Save result in the 'Krometrik_result.csv' file
    public void writeResult() {
        String dir = Environment.getExternalStorageDirectory().toString();
        String fileName = "Krometriks_results.csv";
        String filePath = dir + File.separator + fileName;
        FileWriter mFileWriter;
        CSVWriter writer;

        try {
            File f = new File(filePath);

            if(f.exists() && !f.isDirectory()) {
                mFileWriter = new FileWriter(filePath, true);
                writer = new CSVWriter(mFileWriter);
            }
            else {
                writer = new CSVWriter(new FileWriter(filePath));
                String[] data = {"Date & Time", "Sample Id", "Concentration", "Calibration"};
                writer.writeNext(data);
            }


            String[] data = new String[4];

            Date currentTime = Calendar.getInstance().getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));


            data[0] = String.valueOf(sdf.format(currentTime));
            data[1] = SampleId.getText().toString();
            data[2] = String.valueOf(resultVal + " " + units);
            data[3] = curveName;

            writer.writeNext(data);
            writer.close();
        }


        catch (Exception e) {
            e.printStackTrace();
        }
        Toast.makeText(getApplicationContext(), filePath.toString() + "saved",
                Toast.LENGTH_LONG).show();


    }

    public String getCurve(int n)
    {

        int count=0;
        String line="";
        String name="";
        //String dir = Environment.getExternalStorageDirectory().toString();
        String fileName = "Calibrations.csv";
        //String filePath = dir+File.separator+ fileName;
        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), fileName);
        try {

            BufferedReader inputReader = new BufferedReader(new FileReader(file));
            while ((line = inputReader.readLine()) != null) {
                count++;
                if (count == 1)
                {
                    line = line.replace("\"", "");
                    name = line.split(",")[0];
                }
                if (count != n)
                    continue;

                line = line.replace("\"", "");
                name = line.split(",")[0];
                break;
            }
        }

        catch (IOException e){
            e.printStackTrace();
        }

        return name;
    }

    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.BackMainMenu:

                Intent in_back = new Intent(this,MainActivity.class);

                startActivity(in_back);

                break;

            case R.id.RedoSample:

                Intent in_another = new Intent(this,SampleAnalysis.class);

                startActivity(in_another);

                break;

            case R.id.SaveResult:

                writeResult();

                break;




        }

        }

            @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_value, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
