package com.example.tusky.colorchange;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;


//Class to generate the view containing the ROI (region of interest) and the reference ROI.
//The ROI is denoted by a red rectangle and reference ROI is denoted by a blue rectangle on the preview.
//The ROI and reference ROI coordinates are referenced using a horizontal and vertical adjustment parameter from
//the center of the screen. The adjustment parameters and ROI dimensions are in pixel units.
//This view is overlaid on the camera preview.


public class BoxView extends View {
    Paint paint = new Paint();

    int width, height;

    //Parameters and coordinates for the ROI and reference ROI
    static int box_width, box_height, adj_hor, adj_ver;
    static int ref_size, ref_adj_hor, ref_adj_ver;

    public BoxView(Context context) {

        super(context);

    }

    public BoxView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getRealSize(size);

        //Get width and height of the screen
        width = size.x;
        height = size.y;

    }


    @Override
    public void onDraw(Canvas canvas) {

        int stroke =10;
        float left, right, top, bottom;
        float ref_left, ref_right, ref_top, ref_bottom;

        //The scale factor accounts for differences in the screen and camera preview sizes
        float scale_factor = (float)height/CameraActivity.previewHeight;

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stroke);

        //Reference ROI dimensions and adjustment parameters
        ref_size = 35;
        ref_adj_hor = 150;
        ref_adj_ver = -7;

        //Draw the reference ROI as a rectangle
        ref_left = width/2 - (ref_size/2 + ref_adj_hor)*scale_factor - stroke/2;
        ref_right = ref_left + ref_size*scale_factor + stroke;
        ref_top = height/2 - (ref_size/2 + ref_adj_ver)*scale_factor - stroke/2;
        ref_bottom = ref_top + ref_size*scale_factor + stroke;

        Log.d("Canvas reference", ref_left + " " + ref_right + " " + ref_top + " "+ ref_bottom+" "+scale_factor);

        paint.setColor(Color.BLUE);
        canvas.drawRect(ref_left, ref_top, ref_right, ref_bottom, paint);

        if (Calibration.TAKE_PICTURE!=0)//In all other cases when image capture is not to set the reference
        {
            //ROI dimensions and adjustment parameters
            box_width = 20;
            box_height = 40;
            adj_hor = -63;
            adj_ver = -7;

            //Draw the ROI as a rectangle
            left = width/2 - (box_width/2 + adj_hor)*scale_factor - stroke/2;
            right = left + (box_width-1)*scale_factor + stroke;
            top = height/2 - (box_height/2 + adj_ver)*scale_factor - stroke/2;
            bottom = top + (box_height-1)*scale_factor + stroke;

            Log.d("Canvas", left + " " + right + " " + top + " "+ bottom+" "+scale_factor);

            paint.setColor(Color.RED);
            canvas.drawRect(left, top, right, bottom, paint);
        }


    }

}