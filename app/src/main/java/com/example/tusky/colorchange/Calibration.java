package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;


//This activity shows the page with options related to calibration inlcuding creating a new calibration, selecting
//a calibration or setting a reference

public class Calibration extends Activity implements View.OnClickListener {

    Button CalibrationSelect, CalibrationNew, SetReference;

    static int TAKE_PICTURE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calibration);

        CalibrationNew= (Button) findViewById(R.id.CalibrationNew);
        CalibrationNew.setOnClickListener(this);
        CalibrationNew.setBackgroundColor(Color.rgb(253, 174, 97));

        CalibrationSelect = (Button) findViewById(R.id.CalibrationSelect);
        CalibrationSelect.setOnClickListener(this);
        CalibrationSelect.setBackgroundColor(Color.rgb(171, 217, 233));

        //The 'Set Reference' option is used to set the RGB color values of the reference background screen
        //by acquiring image data in a well lit environment without the diffuser plate.
        //These reference values will serve as reference ROI truth values and will be  used to adjust
        //for lighting intensity variations between measurements.

        SetReference = (Button) findViewById(R.id.SetReference);
        SetReference.setOnClickListener(this);
        SetReference.setBackgroundColor(Color.rgb(211, 211, 211));


    }

    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.CalibrationNew:

                //Move to the create new calibration page using intent
                Intent in_newcalib = new Intent(this, NewCalibration.class);

                startActivity(in_newcalib);

                break;


            case R.id.CalibrationSelect:

                //Move to the select calibration page using intent
                Intent in_selectcalib = new Intent(this,SelectCalibration.class);

                startActivity(in_selectcalib);

                break;

            case R.id.SetReference:

                //Open the camera using intent
                Intent intent_camera = new Intent(this, CameraActivity.class);
                TAKE_PICTURE=0;
                startActivityForResult(intent_camera, TAKE_PICTURE);

                break;

            default:
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK ) {

            //If the reference value data was successfully captured from the camera, they are stored
            //in a file
            Bundle b=intent.getExtras();
            double[] Ref_vals = b.getDoubleArray("RGB");

            writeData(Ref_vals);
            Toast.makeText(Calibration.this, "Reference values stored", Toast.LENGTH_LONG).show();

        }
    }


    //Store the reference value data in a file
    public void writeData(double val[])
    {
        String dir = Environment.getExternalStorageDirectory().toString();
        String fileName = "Reference_Values.csv";
        String filePath = dir+ File.separator+ fileName;
        CSVWriter writer;
;
        try{
            writer = new CSVWriter(new FileWriter(filePath));

            String[] data = new String[3];
            for (int i =0; i<3; i++)
                data[i] = String.valueOf(val[i]);

            writer.writeNext(data);

            writer.close();

        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calibration_curve, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
