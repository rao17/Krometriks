package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Environment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

//This activity allows the user to select from a list of stored calibrations.
//The calibration names are stored in the file 'Calibrations.csv'. Each name from this file is extracted
//and shown to the user as a radio button. The user can select one option. The selected curve is noted
//for future use.

public class SelectCalibration extends Activity implements View.OnClickListener {

    RadioButton Curve;
    TextView SelectCurveText;
    Button SelectDone;
    int count=0;

//    ArrayList <double []> values = new ArrayList<double []>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_calibration);

        SelectDone = (Button) findViewById(R.id.SelectDone);
        SelectDone.setOnClickListener(this);
        SelectDone.setBackgroundColor(Color.rgb(211, 211, 211));


        SelectCurveText = (TextView) findViewById(R.id.SelectCurveText);

        //Get data from Calibrations.csv' file
        String line;
        String fileName = "Calibrations.csv";

        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), fileName);
        try {

            BufferedReader inputReader = new BufferedReader(new FileReader(file));
            while ((line = inputReader.readLine()) != null) {
                line= line.replace("\"", "");
                String[] vals = line.split(",");

                count++;

                //Add radio button for each stored calibration
                addRadioButton(vals[0], count);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }


    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.SelectDone:

                int i, curveNum = 1;
                for (i =1; i<=count; i++)
                {
                    Curve =(RadioButton) findViewById(i);
                    if (Curve.isChecked())
                    {
                        curveNum = i;
                        Toast.makeText(getApplicationContext(), Curve.getText().toString() +" curve selected",
                                Toast.LENGTH_LONG).show();
                        break;
                    }
                }


                //Note details of the selected calibration
                SharedPreferences pref = getApplicationContext().getSharedPreferences("Pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt("CurveNumber", curveNum);
                editor.commit(); // commit changes

                //Go back to the main menu
                Intent intent_back = new Intent(this, MainActivity.class);
                startActivity(intent_back);
                break;
        }
    }

    //Add a radio button to the list
    public void addRadioButton(String name, int count) {

        RadioButton radioButton = new RadioButton(this);
        radioButton.setId(count);
        radioButton.setText(name);
        radioButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
        if (count%2!=0)
            radioButton.setBackgroundColor(Color.rgb(171, 217, 233));
        else
            radioButton.setBackgroundColor(Color.rgb(253, 174, 97));

        RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.MATCH_PARENT,
                RadioGroup.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(dpToPx(0), dpToPx(10), dpToPx(0), dpToPx(10));
        radioButton.setLayoutParams(params);

        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);
        rg.addView(radioButton);
    }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_select_curve, menu);
        return true;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
