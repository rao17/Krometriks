package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

//This activity creates a new calibration curve

public class NewCalibration extends Activity implements View.OnClickListener {

    //Variables for the different components on the page
    Button CalibrationPicture, CalibrationSavePoint, CalibrationDone;
    EditText CalibrationName, CalibrationValue;
    Spinner ValueUnits;

    ArrayList<Double> values_list = new ArrayList<Double>();
    ArrayList<Double> R_list = new ArrayList<Double>();
    ArrayList<Double> G_list = new ArrayList<Double>();
    ArrayList<Double> B_list = new ArrayList<Double>();

    double[] RGBvalue = new double [3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_calibration);

        CalibrationName = (EditText) findViewById(R.id.CalibrationName);

        CalibrationPicture = (Button) findViewById(R.id.CalibrationImage);
        CalibrationPicture.setOnClickListener(this);
        CalibrationPicture.setBackgroundColor(Color.rgb(171, 217, 233));

        CalibrationValue = (EditText) findViewById(R.id.CalibrationValue);

        CalibrationSavePoint = (Button)findViewById(R.id.CalibrationSavePoint);
        CalibrationSavePoint.setOnClickListener(this);
        CalibrationSavePoint.setBackgroundColor(Color.rgb(253, 174, 97));

        CalibrationDone = (Button) findViewById(R.id.CalibrationDone);
        CalibrationDone.setOnClickListener(this);
        CalibrationDone.setBackgroundColor(Color.rgb(211, 211, 211));


        ValueUnits = (Spinner) findViewById(R.id.ValueUnits);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.value_units, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        ValueUnits.setAdapter(adapter);

        ValueUnits.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Default RGB image data value
        for (int i =0; i<3; i++)
            RGBvalue[i] = -1;
       }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_curve, menu);
        return true;

    }

    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.CalibrationImage:

                //Open the camera
                Intent intent_camera1 = new Intent(this,CameraActivity.class);

                Calibration.TAKE_PICTURE = 1;
                startActivityForResult(intent_camera1, Calibration.TAKE_PICTURE);

                break;


            case R.id.CalibrationSavePoint:

                //Save image data as a calibration point
                addToList();

                break;

            case R.id.CalibrationDone:

                //Save calibration data to a file
                writeData();

                Intent intent_back = new Intent(this,Calibration.class);
                startActivity(intent_back);

                break;

        }
    }

    //Convert RGB values to Lab values
    public double[][] RGBtoLab(double[][] RGB)
    {
        double[] ref_D65 = {95.047, 100.0, 108.883};
        double[][] weights = {{0.4124, 0.3576, 0.1805},{0.2126, 0.7152, 0.0722}, {0.0193, 0.1192, 0.9505}};
        double condition = Math.pow(((double)6/29),3);
        double val = Math.pow(((double)29/6), 2)/3;

        int size = RGB.length;
        double [][] RGBn = new double[size][3];
        double [][] RGBl = new double[size][3];

        for (int i=0; i<size; i++)
        {
            for (int j=0;j<3;j++)
            {
                RGBn[i][j] = RGB[i][j]/255;
                if (RGBn[i][j]>0.04045)
                {
                    RGBl[i][j] = Math.pow((0.055 + RGBn[i][j])/1.055, 2.4);
                }
                else
                {
                    RGBl[i][j] = RGBn[i][j]/12.92;
                }

            }
        }

        double[][] XYZ = new double[size][3];
        double[][] XYZn = new double[size][3];
        double[][] func = new double[size][3];
        for (int i =0; i<size;i++)
        {
            for (int j=0;j<3;j++)
            {
                XYZ[i][j]=0;

                for (int k=0; k<3;k++) {

                    XYZ[i][j] += RGBl[i][k] * weights[j][k];
                }

                XYZn[i][j] = 100*XYZ[i][j]/ref_D65[j];

                if (XYZn[i][j] > condition)
                {
                    func[i][j] = Math.pow(XYZn[i][j],((double)1/3));
                }
                else
                {
                    func[i][j] = val *XYZn[i][j]+((double)4/29);
                }

            }

        }

        double[][] Lab = new double[size][3];

        for (int i=0; i<size;i++)
        {
            Lab[i][0] = 116 * func[i][1] -16;
            Lab[i][1] = 500 * (func[i][0] - func[i][1]);
            Lab[i][2] = 200 * (func[i][1] - func[i][2]);
        }

        return Lab;

    }


    //Add RGB data and concentration data for calibration point to the list of calibration points
    public void addToList()
    {
        Double value = Double.parseDouble(CalibrationValue.getText().toString());
        String unit = ValueUnits.getSelectedItem().toString();

        switch(unit){
            case "M": value = value * Math.pow(10,6);
                break;
            case "mM": value = value * Math.pow(10,3);
                break;
            case "\u00B5M": value = value * Math.pow(10,0);
                break;
            case "nM": value = value * Math.pow(10,-3);
                break;
            case "pM": value = value * Math.pow(10,-6);
                break;
            default: break;
        }

        R_list.add(RGBvalue[0]);
        G_list.add(RGBvalue[1]);
        B_list.add(RGBvalue[2]);

        values_list.add(value);

        CalibrationValue.setText(null);


    }


    //Sort calibration points based on concentration values in ascending order
    public double[][] sortValues(Double[] values, double[][] RGB)
    {
        int swap = 1;
        int n = values.length;
        while (swap > 0 && n > 1){
            swap = 0;
            for (int i =0; i<n-1; i++)
            {
                if (values[i] > values[i+1])
                {
                    swap = swap+1;

                    values[i] = values[i] + values[i+1];
                    values[i+1] = values[i] - values[i+1];
                    values[i] = values[i] - values[i+1];

                    for (int j =0; j<3; j++)
                    {
                        RGB[i][j] = RGB[i][j] + RGB[i+1][j];
                        RGB[i+1][j] = RGB[i][j] - RGB[i+1][j];
                        RGB[i][j] = RGB[i][j] - RGB[i+1][j];

                    }

                }
            }
            n = n-1;
        }
        return RGB;
    }


    //Store calibration data to files
    public void writeData()
    {
        Double[] values = new Double[values_list.size()];
        values = values_list.toArray(values);

        Double[] R = new Double[R_list.size()];
        R = R_list.toArray(R);

        Double[] G = new Double[G_list.size()];
        G = G_list.toArray(G);

        Double[] B = new Double[B_list.size()];
        B = B_list.toArray(B);

        double[][] RGB = new double [R.length][3];
        for (int i =0; i<R.length; i++)
        {
            RGB[i][0] = R[i];
            RGB[i][1] = G[i];
            RGB[i][2] = B[i];
        }

        double[][] sorted_RGB = sortValues(values, RGB);
        Double[] sorted_values = values;

        double[][] sorted_Lab = RGBtoLab(sorted_RGB);


        //Store sorted calibration points list to a file

        String dir = Environment.getExternalStorageDirectory().toString();
        String fileName = CalibrationName.getText().toString()+".csv";
        String filePath = dir+File.separator+ fileName;
        FileWriter mFileWriter;
        CSVWriter writer;

        try{
            writer = new CSVWriter(new FileWriter(filePath));

            String[][] data = new String[sorted_Lab.length][7];
            for(int i=0;i<sorted_Lab.length;i++)
            {
                for (int j=0; j<7; j++) {
                    if (j == 3)
                        data[i][j] = String.valueOf(sorted_values[i]);
                    else if (j>=4)
                        data[i][j] = String.valueOf(sorted_RGB[i][j-4]);
                    else
                        data[i][j] = String.valueOf(sorted_Lab[i][j]);
                }

                writer.writeNext(data[i]);
            }

            writer.close();
            Toast.makeText(getApplicationContext(), filePath+"saved",
                    Toast.LENGTH_LONG).show();


            //Store calibration curve name to the file containing the list of calibrations
            String fileName2 = "Calibrations.csv";
            String filePath2 = dir + File.separator + fileName2;
            File f = new File(filePath2);

                    if(f.exists() && !f.isDirectory()) {
                        mFileWriter = new FileWriter(filePath2, true);
                        writer = new CSVWriter(mFileWriter);
                    }
                    else {
            writer = new CSVWriter(new FileWriter(filePath2));
                    }

            String[] data2 = {CalibrationName.getText().toString()};
            writer.writeNext(data2);

            writer.close();


        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == Activity.RESULT_OK )
        {
            //If image data capture from the camera is successful, extract the RGB data
            Bundle b=intent.getExtras();
            RGBvalue=b.getDoubleArray("RGB");

        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
