package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


//This activity shows the first/main page of the app with options for calibration and sample analysis

public class MainActivity extends Activity implements View.OnClickListener {

    Button CalibrationButton, AnalysisButton, DiagnosticButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button for calibration
        CalibrationButton = (Button) findViewById(R.id.CalibrationButton);
        CalibrationButton.setOnClickListener(this);
        CalibrationButton.setBackgroundColor(Color.rgb(171, 217, 233));

        //Button for sample analysis
        AnalysisButton = (Button) findViewById(R.id.SampleAnalysisButton);
        AnalysisButton.setOnClickListener(this);
        AnalysisButton.setBackgroundColor(Color.rgb(253, 174, 97));

    }


    @Override
    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.CalibrationButton:

                //Move to the page with calibration options using intent
                Intent in_calibration = new Intent(this,Calibration.class);

                startActivity(in_calibration);

                break;

            case R.id.SampleAnalysisButton:

                //Move to the page with sample analysis options using intent
                Intent in_sampleanalysis = new Intent(this,SampleAnalysis.class);

                startActivity(in_sampleanalysis);

            default:
                break;

        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
