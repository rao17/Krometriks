package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.opencsv.CSVWriter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


//This activity shows the camera preview overlaid with markings for the ROI and the reference ROI.
//The user can capture image data using this page. The pixel data is converted to a single averaged
//normalized RGB value for each measurement (consisting of a set of 11 images).

//The images are never stored. Just the pixel data from the relevant regions is extracted and after
//processing the single RGB value for the measurement is stored.

public class CameraActivity extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, View.OnClickListener {

    Camera camera;
    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    LayoutInflater controlInflater = null;

    Button RecaptureButton, DoneButton, SetFocusButton;
    TextView RGBValues;

    static int previewHeight;


    int counter = -1, count = 0;

    //List of RGB values for pixels in the ROI
    ArrayList<Double> R_list = new ArrayList<Double>();
    ArrayList<Double> G_list = new ArrayList<Double>();
    ArrayList<Double> B_list = new ArrayList<Double>();

    //List of RGB values for pixels in the reference ROI
    ArrayList<Double> ref_R_list = new ArrayList<Double>();
    ArrayList<Double> ref_G_list = new ArrayList<Double>();
    ArrayList<Double> ref_B_list = new ArrayList<Double>();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get the camera preview on the full screen
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);

        setContentView(R.layout.activity_camera);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
        surfaceHolder = surfaceView.getHolder();

        //Overlay the view showing the ROI and reference ROI onto the camera preview
        controlInflater = LayoutInflater.from(getBaseContext());
        View viewControl = controlInflater.inflate(R.layout.captureimage, null);
        ViewGroup.LayoutParams layoutParamsControl
                = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
                ViewGroup.LayoutParams.FILL_PARENT);
        this.addContentView(viewControl, layoutParamsControl);

        RecaptureButton = (Button) findViewById(R.id.RecaptureButton);
        RecaptureButton.setBackgroundColor(Color.rgb(171, 217, 233));
        RecaptureButton.setOnClickListener(this);
        RecaptureButton.setVisibility(View.INVISIBLE);
        DoneButton = (Button) findViewById(R.id.DoneButton);
        DoneButton.setBackgroundColor(Color.rgb(253, 174, 97));
        DoneButton.setOnClickListener(this);
        DoneButton.setVisibility(View.INVISIBLE);
        SetFocusButton = (Button) findViewById(R.id.SetFocusButton);
        SetFocusButton.setOnClickListener(this);
        RGBValues = (TextView) findViewById(R.id.RGBValues);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        surfaceHolder.addCallback(this);

        // deprecated setting, but required on Android versions prior to 3.0
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.SetFocusButton:

                setCameraFocus();
                break;


            case R.id.RecaptureButton:

                captureImage();
                break;


            case R.id.DoneButton:

                finishActivity();
                break;

            default:
                break;

        }

    }



    //Check if camera is in focus and if yes, then begin a time-delayed image capture
    public void setCameraFocus() {
        camera.autoFocus(new Camera.AutoFocusCallback() {
            public void onAutoFocus(boolean success, Camera camera) {
                if (success) {
                    setFlashOn();
                    count = 1;
                    Toast.makeText(CameraActivity.this, "Focus is set", Toast.LENGTH_SHORT).show();

                    //Image capture begins a few seconds after the camera is focused
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            captureImage();
                        }
                    }, 7000);

                }
            }
        });



    }

    //Set the camera flash to the on state
    public void setFlashOn() {

        Camera.Parameters param;
        param = camera.getParameters();
        param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        camera.setParameters(param);

    }

    //Reset all RGB value variables to zero
    public void captureImage() {
        counter = 0;
        R_list.clear();
        G_list.clear();
        B_list.clear();

        ref_R_list.clear();
        ref_G_list.clear();
        ref_B_list.clear();
    }

    //Once image capture is complete and the user presses 'Done'
    //Image data processing
    public void finishActivity() {

        Double[] R = new Double[R_list.size()];
        R = R_list.toArray(R);

        Double[] G = new Double[G_list.size()];
        G = G_list.toArray(G);

        Double[] B = new Double[B_list.size()];
        B = B_list.toArray(B);


        double[] RGB_val = new double[3];

        if (R_list.size() != 0) {

            double[] r = new double[R.length];
            double[] g = new double[G.length];
            double[] b = new double[B.length];

            Double[] ref_R = new Double[ref_R_list.size()];
            Double[] ref_G = new Double[ref_G_list.size()];
            Double[] ref_B = new Double[ref_B_list.size()];


            //For all cases except setting of the reference values
            if (Calibration.TAKE_PICTURE != 0)
            {

                ref_R = ref_R_list.toArray(ref_R);
                ref_G = ref_G_list.toArray(ref_G);
                ref_B = ref_B_list.toArray(ref_B);

                double [] ref_vals = getReferenceValues();

                //Normalzie the ROI RGB values with the reference ROI values and the reference truth values
                double[] norm_R = new double[R.length];
                double[] norm_G = new double[G.length];
                double[] norm_B = new double[B.length];

                for (int i =0; i<R.length; i++)
                {
                    norm_R[i] = R[i]/ref_R[i]*ref_vals[0];
                    norm_G[i] = G[i]/ref_G[i]*ref_vals[1];
                    norm_B[i] = B[i]/ref_B[i]*ref_vals[2];
                }


                //Get the average of the normalized values
                RGB_val[0] = getAvg(norm_R);
                RGB_val[1] = getAvg(norm_G);
                RGB_val[2] = getAvg(norm_B);

            }

            //For the case involving the setting of the reference values
            else
            {
                //Get the average of the normalized values
                RGB_val[0] = getAvg(R);
                RGB_val[1] = getAvg(G);
                RGB_val[2] = getAvg(B);
            }

            for (int i =0; i<3; i++)
                RGB_val[i] = roundDecimal(RGB_val[i], 3);

            //For testing
            //Writing the RGB values acquired from the ROI and reference ROI to a file
//            if (Calibration.TAKE_PICTURE != 0)
//                writeData(R, G, B, ref_R, ref_G, ref_B);
//            else
//                writeData(R, G, B, R, G, B);

        }

        //If image capture is unsuccessful
        else

        {
            RGB_val[0] = -1;
            RGB_val[1] = -1;
            RGB_val[2] = -1;
        }

        Toast.makeText(CameraActivity.this, "R = " + RGB_val[0] + " " + "G = " + RGB_val[1] + " " +
                "B = " + RGB_val[2], Toast.LENGTH_LONG).show();

        //Send back the averaged RGB values to the activity from which the camera opening intent arose
        Bundle b = new Bundle();
        b.putDoubleArray("RGB", RGB_val);
        Intent resultIntent = new Intent();
        resultIntent.putExtras(b);
        setResult(Activity.RESULT_OK, resultIntent);

        finish();

    }

    //Round a decimal number to a given number of digits
    public double roundDecimal(double val, int n)
    {
        double new_num = val*Math.pow(10, n);
        new_num = Math.round(new_num);
        return (new_num/Math.pow(10,n));
    }

    public void refreshCamera() {
        if (surfaceHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            camera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here
        // start preview with new settings
        try {

            camera.setPreviewDisplay(surfaceHolder);
            camera.setPreviewCallback(this);
            camera.startPreview();
        } catch (Exception e) {

        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        refreshCamera();
    }


    //Handler thread
    //Separate thread to open the camera so as to not clog the UI thread
    private CameraHandlerThread thread = null;

    private class CameraHandlerThread extends HandlerThread {
        Handler handler = null;

        CameraHandlerThread() {
            super("CameraHandlerThread");
            start();
            handler = new Handler(getLooper());
        }

        synchronized void notifyCameraOpened() {
            notify();
        }

        void cameraOpen() {
            handler.post(new Runnable() {
                @Override
                public void run() {

                    try {
                        camera = Camera.open();
                    }
                    catch (RuntimeException e) {
                        System.err.println(e);
                    }
                    notifyCameraOpened();
                }
            });
            try {
                wait();
            }
            catch (InterruptedException e) {
                System.err.println(e);
            }
        }
    }


    public void openCamera() {
        if (thread == null) {
            thread = new CameraHandlerThread();
        }

        synchronized (thread) {
            thread.cameraOpen();
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {

        openCamera();
        setCameraParams();


        try {
            // The Surface has been created, now tell the camera where to draw
            // the preview.
            camera.setPreviewDisplay(surfaceHolder);
            camera.setPreviewCallback(this);
            camera.startPreview();
        } catch (Exception e) {
            // check for exceptions
            System.err.println(e);
            return;
        }


    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // stop preview and release camera
        camera.stopPreview();
        camera.setPreviewCallback(null);
        camera.release();
        camera = null;
    }

    @Override
    public void onPause() {
        super.onPause();
        camera.stopPreview();
        camera.setPreviewCallback(null);
    }

    @Override
    public void onResume() {
        super.onResume();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //
                View decorView = getWindow().getDecorView();
                decorView.setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                                | View.SYSTEM_UI_FLAG_IMMERSIVE);




            }
        });

        refreshCamera();

    }


    //Set the parameters for the camera
    public void setCameraParams()
    {
        Display display = getWindowManager().getDefaultDisplay();
        Point s = new Point();
        display.getRealSize(s);
        int screen_width = s.x;
        int screen_height = s.y;
        float screen_AR = (float) screen_height / screen_width;

        Camera.Parameters param;
        param = camera.getParameters();


        List<Camera.Size> suppPreviewSizes = param.getSupportedPreviewSizes();

        Camera.Size previewSize = suppPreviewSizes.get(0);

        int count = 0;
        float temp_prev_AR;
        for (int i = 0; i < suppPreviewSizes.size(); i++) {

            temp_prev_AR = (float) suppPreviewSizes.get(i).width / suppPreviewSizes.get(i).height;
            if (Math.abs(temp_prev_AR - screen_AR) < 0.0005) {

                previewSize = suppPreviewSizes.get(i);
                previewHeight = previewSize.width;
                count = 1;
                break;
            }

        }


        if (count == 1) {

            //For testing
            //To check screen and preview sizes
//          Log.d("Screen:", screen_width + "  " + screen_height);
//          Log.d("Preview:", previewSize.width + "  " + previewSize.height);

            camera.setDisplayOrientation(90);
            param.setPreviewFormat(ImageFormat.NV21);
            param.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            param.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
            param.setRotation(90);


            param.setPreviewSize(previewSize.width, previewSize.height);

            camera.setParameters(param);



        } else {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(CameraActivity.this, "Error opening camera preview", Toast.LENGTH_LONG).show();
                    finish();
                }
            });

        }


        if (Calibration.TAKE_PICTURE!=0)
        {
            double[] vals = getReferenceValues();
            double sum=0;
            for (int i=0; i<3;i++)
                sum = sum+ vals[i];

            //If reference has not been set and calibration creation or sample analysis is being performed
            if (sum==-3)
            {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CameraActivity.this, "Set reference", Toast.LENGTH_LONG).show();
                        finish();

                    }
                });


            }

        }


    }

    //Get the reference ROI truth values
    public double[] getReferenceValues()
    {
        String line;
        String fileName = "Reference_Values.csv";
        double values[] = new double[3];
        for(int i =0;i<3;i++)
            values[i] = -1;


        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), fileName);
        if (!file.exists())
            return values;

        try {

            BufferedReader inputReader = new BufferedReader(new FileReader(file));
            while ((line = inputReader.readLine()) != null) {
                line= line.replace("\"", "");
                String[] vals = line.split(",");
                for (int i =0; i<vals.length; i++)
                    values[i] = Double.parseDouble(vals[i]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return values;

    }


    long tStart;

    //For each preview frame, acquire pixel data for the ROI and the reference ROI
    @Override
    public void onPreviewFrame(final byte[] data, Camera camera) {

        if (counter == 0) {
            tStart = System.currentTimeMillis();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Pixel capture started",
                            Toast.LENGTH_LONG).show();

                }
            });

        }

        //Do this for 11 frames i.e 11 images
        if (counter > -1 && counter < 11) {

            //transforms NV21 pixel data into RGB pixels
            int w = camera.getParameters().getPreviewSize().height;
            int h = camera.getParameters().getPreviewSize().width;

            int[] Hex = decodeYUV420SP(data, h, w);

            int[] rotated_Hex = rotateImage(Hex, 90);

            int ref_size = BoxView.ref_size;
            int ref_ver_adj = BoxView.ref_adj_ver;
            int ref_hor_adj = BoxView.ref_adj_hor;

            int[][] ref_RGB = new int[3][ref_size * ref_size];

            int count = 0;

            count = 0;
            for (int i = 0; i < ref_size; i++) {
                for (int j = 0; j < ref_size; j++) {
                    int pos = w * (h / 2 - ref_ver_adj - 1 - ref_size / 2 + i) + (w / 2 - ref_hor_adj - 1 - ref_size / 2 + j);
                    int[] RGB_temp = HextoRGB(rotated_Hex[pos]);
                    for (int k = 0; k < 3; k++) {
                        ref_RGB[k][count] = RGB_temp[k];
                    }
                    count++;
                }
            }


            final double ref_RGB_Avg[] = new double[3];

            for (int i = 0; i < 3; i++)
                ref_RGB_Avg[i] = getAvg(ref_RGB[i]);


            if (Calibration.TAKE_PICTURE != 0) {
                int box_width = BoxView.box_width;
                int box_height = BoxView.box_height;
                int ver_adj = BoxView.adj_ver;
                int hor_adj = BoxView.adj_hor;

                int[][] RGB = new int[3][box_width * box_height];


                count = 0;

                for (int i = 0; i < box_height; i++) {
                    for (int j = 0; j < box_width; j++) {
                        int pos = w * (h / 2 - ver_adj - (box_height / 2) - 1 + i) + (w / 2 - hor_adj - (box_width / 2) - 1 + j);

                        int[] RGB_temp = HextoRGB(rotated_Hex[pos]);
                        for (int k = 0; k < 3; k++) {
                            RGB[k][count] = RGB_temp[k];
                        }
                        count++;

                    }

                }

                final double RGB_Avg[] = new double[3];
                for (int i = 0; i < 3; i++)
                    RGB_Avg[i] = getAvg(RGB[i]);

                //For testing
//               //Show the RGB values acquired.
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        RGBValues.setText("R = " + RGB_Avg[0] + " G = " + RGB_Avg[1] + " B = " + RGB_Avg[2] + " " + counter);
//
//
//                    }
//                });

                R_list.add(RGB_Avg[0]);
                G_list.add(RGB_Avg[1]);
                B_list.add(RGB_Avg[2]);
                ref_R_list.add(ref_RGB_Avg[0]);
                ref_G_list.add(ref_RGB_Avg[1]);
                ref_B_list.add(ref_RGB_Avg[2]);

            } else

            {
                //For testing
                //Show the RGB values acquired.
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//
//                        RGBValues.setText("R = " + ref_RGB_Avg[0] + " G = " + ref_RGB_Avg[1] + " B = " + ref_RGB_Avg[2] + " " + counter);
//
//
//                    }
//                });
//

                R_list.add(ref_RGB_Avg[0]);
                G_list.add(ref_RGB_Avg[1]);
                B_list.add(ref_RGB_Avg[2]);

            }

            counter++;


        } else if (counter == 11) {

            //For testing
            //Calculate time for pixel capture
//            long tEnd = System.currentTimeMillis();
//            long tDelta = tEnd - tStart;
//            final double elapsedSeconds = tDelta / 1000.0;


            //Once the pixel capture is complete
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "Pixels captured ",
                            Toast.LENGTH_LONG).show();

                    RecaptureButton.setVisibility(View.VISIBLE);
                    DoneButton.setVisibility(View.VISIBLE);
                    SetFocusButton.setVisibility(View.INVISIBLE);


                }
            });

            counter++;

        }

    }

    //Rotate the image captured as defualt capture orientation is landscape mode and the image preview is in
    //portrait mode
    public int[] rotateImage(final int[] data, int angle) {

        Camera.Size previewSize = camera.getParameters().getPreviewSize();
        Bitmap bmp = Bitmap.createBitmap(data, previewSize.width, previewSize.height, Bitmap.Config.ARGB_8888);

        // Rotate the Bitmap
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);


        bmp = Bitmap.createBitmap(bmp, 0, 0, previewSize.width, previewSize.height, matrix, false);

        int[] rotated_data = new int[bmp.getWidth() * bmp.getHeight()];
        bmp.getPixels(rotated_data, 0, bmp.getWidth(), 0, 0, bmp.getWidth(), bmp.getHeight());

        bmp.recycle();
        return rotated_data;
    }

    //Get the average value from an array of numbers of type Double
    public double getAvg(Double a[]) {
        double sum = 0;
        double avg;
        for (int i = 0; i < a.length; i++) {
            sum = sum + a[i];
        }

        avg = sum / a.length;

        return avg;
    }

    //Get the average value from an array of numbers of type double
    public double getAvg(double a[]) {
        double sum = 0;
        double avg;
        for (int i = 0; i < a.length; i++) {
            sum = sum + a[i];
        }

        avg = sum / a.length;

        return avg;
    }


    //Get the average value from an array of integers
    public double getAvg(int a[]) {
        double sum = 0;
        double avg;
        for (int i = 0; i < a.length; i++) {
            sum = sum + a[i];
        }

        avg = sum / a.length;

        return avg;
    }


    //Convert hexadecimal color value to RGB color value
    public int[] HextoRGB(int Hex) {
        int RGB[] = new int[3];
        RGB[0] = ((Hex >> 16) & 0xff);
        RGB[1] = ((Hex >> 8) & 0xff);
        RGB[2] = (Hex & 0xff);
        return RGB;
    }

    //Decode the pixels from preview to the hexadecimal form
    public int[] decodeYUV420SP(byte[] yuv420sp, int width, int height) {

        final int frameSize = width * height;

        int[] rgb = new int[frameSize];
        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
            for (int i = 0; i < width; i++, yp++) {
                int y = (0xff & ((int) yuv420sp[yp])) - 16;
                if (y < 0)
                    y = 0;
                if ((i & 1) == 0) {
                    v = (0xff & yuv420sp[uvp++]) - 128;
                    u = (0xff & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                if (r < 0) r = 0;
                else if (r > 262143)
                    r = 262143;
                if (g < 0) g = 0;
                else if (g > 262143)
                    g = 262143;
                if (b < 0) b = 0;
                else if (b > 262143)
                    b = 262143;

                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
            }
        }

        return rgb;
    }

    //For testing
    //Write out a set of RGB image values of the ROI and the reference ROI

//    public void writeData(Double[] R, Double[] G, Double[] B, Double[] ref_R, Double[] ref_G, Double[] ref_B) {
//
//
//        String dir = Environment.getExternalStorageDirectory().toString();
//        String fileName = "Test.csv";
//
//        String filePath = dir + File.separator + fileName;
//        CSVWriter writer;
//
//        try {
//            writer = new CSVWriter(new FileWriter(filePath));
//
//            String[][] data = new String[R.length][6];
//            for (int i = 0; i < R.length; i++) {
//                data[i][0] = String.valueOf(R[i]);
//                data[i][1] = String.valueOf(G[i]);
//                data[i][2] = String.valueOf(B[i]);
//                data[i][3] = String.valueOf(ref_R[i]);
//                data[i][4] = String.valueOf(ref_G[i]);
//                data[i][5] = String.valueOf(ref_B[i]);
//                writer.writeNext(data[i]);
//            }
//
//            writer.close();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//
//    }
}

