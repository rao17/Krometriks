package com.example.tusky.colorchange;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Environment;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//Activity that shows the page to perform sample analysis. Options on this page include sample image
//data capture, and analysis. The page also shows at the bottom, the calibration being used for the analysis.

//Each color is denoted by its L*, a* and b* value as a point in 3D space.

// Algorithm for analysis in a nutshell: First, the Euclidean distance between the test point and all the calibration
//points is computed. The closest point is chosen (lets call it x). Then, from the two adjacent (one each side)
//calibrations points to x, the one that is the closest is designated as y. The calibration points have been sorted
//according to concentration value. Let the test point be z. Then, the point on the line joining x and y
//that is closest to z is calculated. This point's location is linearly interpolated on the concentration gradient
//between x and y to get the test sample concentration.


public class SampleAnalysis extends Activity implements View.OnClickListener {

    Button PictureButton, ComputeButton;

    TextView CurveSelected;

    double[] RGB = new double [3];
    String curveName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis_sample);

        PictureButton= (Button) findViewById(R.id.PictureButton);
        PictureButton.setOnClickListener(this);
        PictureButton.setBackgroundColor(Color.rgb(171, 217, 233));

        ComputeButton= (Button) findViewById(R.id.ComputeButton);
        ComputeButton.setOnClickListener(this);
        ComputeButton.setBackgroundColor(Color.rgb(253, 174, 97));

        CurveSelected = (TextView) findViewById(R.id.CurveSelected);
        CurveSelected.setBackgroundColor(Color.TRANSPARENT);
        CurveSelected.setTextColor(Color.rgb(153, 76, 0));


        SharedPreferences pref = getApplicationContext().getSharedPreferences("Pref", MODE_PRIVATE);
        int n = pref.getInt("CurveNumber", 1);

        curveName = getCurve(n);
        CurveSelected.setText("Calibration selected: "+curveName);

        for (int i = 0; i<3; i++)
            RGB[i] = -1;

    }


    public void onClick(View view) {


        switch (view.getId()) {

            case R.id.PictureButton:

                //Open camera using an intent
                Intent intent_camera = new Intent(this,CameraActivity.class);
                Calibration.TAKE_PICTURE = 2;
                startActivityForResult(intent_camera, Calibration.TAKE_PICTURE);

                break;


            case R.id.ComputeButton:

                String result_val = String.valueOf(calConcentration(RGB));

                //Move to the result display page using an intent
                Intent in3 = new Intent(this,DisplayResult.class);
                in3.putExtra("Result",result_val);

                startActivity(in3);

                break;


            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent)
    {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == RESULT_OK ) {

            //If image capture is successful, get region of interest image data
            Bundle b=intent.getExtras();
            RGB=b.getDoubleArray("RGB");

        }
    }

    //Convert RGB values to Lab values
    public double[] RGBtoLab(double[] RGB)
    {
        double[] ref_D65 = {95.047, 100.0, 108.883};
        double[][] weights = {{0.4124, 0.3576, 0.1805},{0.2126, 0.7152, 0.0722}, {0.0193, 0.1192, 0.9505}};
        double condition = Math.pow(((double) 6 / 29), 3);
        double val = Math.pow(((double)29 / 6), 2)/3;



        double [] RGBn = new double[3];
        double [] RGBl = new double[3];


            for (int j=0;j<3;j++)
            {
                RGBn[j] = RGB[j]/255;
                if (RGBn[j]>0.04045)
                {
                    RGBl[j] = Math.pow((0.055 + RGBn[j])/1.055, 2.4);
                }
                else
                {
                    RGBl[j] = RGBn[j]/12.92;
                }
                //RGBl[j] = RGBl[j]*100;
            }


        double[]XYZ = new double[3];
        double[] XYZn = new double[3];
        double[] func = new double[3];

            for (int j=0;j<3;j++)
            {
                XYZ[j]=0;

                for (int k=0; k<3;k++) {

                    XYZ[j] += RGBl[k] * weights[j][k];
                }

                XYZn[j] = 100*XYZ[j]/ref_D65[j];
                if (XYZn[j] > condition)
                {
                    func[j] = Math.pow(XYZn[j],((double)1/3));
                }
                else
                {
                    func[j] = val *XYZn[j]+((double)4/29);
                }

            }



        double[] Lab = new double[3];



            Lab[0] = 116 * func[1] -16;
            Lab[1] = 500 * (func[0] - func[1]);
            Lab[2] = 200 * (func[1] - func[2]);


        return Lab;

    }

    //Calculate the concentration based on algorithm
    public double calConcentration(double [] RGB)
    {


        double[] Lab = RGBtoLab(RGB);
        double Conc =0;

        //Get calibration points from the appropriate file
        String curveFile = curveName+".csv";
        ArrayList<Double> L = new ArrayList<Double>();
        ArrayList<Double> a = new ArrayList<Double>();
        ArrayList<Double> b = new ArrayList<Double>();
        ArrayList<Double> val = new ArrayList<Double>();

        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), curveFile);

        String line="";
        try {

            BufferedReader inputReader = new BufferedReader(new FileReader(file));
            while ((line = inputReader.readLine()) != null) {
                line= line.replace("\"", "");
                String[] vals = line.split(",");
                L.add(Double.parseDouble(vals[0]));
                a.add(Double.parseDouble(vals[1]));
                b.add(Double.parseDouble(vals[2]));
                val.add(Double.parseDouble(vals[3]));
            }

        }
        catch (IOException e){
            e.printStackTrace();
        }


        double temp;
        ArrayList <Double> dist = new ArrayList<Double>();
        for (int i = 0; i<val.size(); i++)
        {
            temp  = Math.sqrt(Math.pow(Lab[0]-L.get(i),2) + Math.pow(Lab[1]-a.get(i),2) + Math.pow(Lab[2]-b.get(i),2));
            dist.add(temp);
        }


        double d_xz, d_yz;
        int ind_x, ind_y;

        ind_x = calMinIndex(dist);

        if(ind_x==0)
            ind_y = 1;
        else if (ind_x==dist.size()-1)
            ind_y = dist.size()-2;
        else
        {
            if (dist.get(ind_x-1) < dist.get(ind_x+1))
                ind_y = ind_x-1;
            else
                ind_y = ind_x+1;
        }

        d_xz = dist.get(ind_x);
        d_yz = dist.get(ind_y);

        double d_xy = Math.sqrt(Math.pow(L.get(ind_x) - L.get(ind_y), 2) +
                        Math.pow(a.get(ind_x) - a.get(ind_y), 2) +
                Math.pow(b.get(ind_x) - b.get(ind_y), 2));

        double C_yx = val.get(ind_y) - val.get(ind_x);

        double D = ((d_xz+d_yz)*(d_xz-d_yz)+Math.pow(d_xy,2))/(2*d_xy);

        Conc = val.get(ind_x) + D/d_xy * C_yx;

        return Conc;
    }

    //Caluculate the index of the minimum value in an array
    public int calMinIndex(ArrayList <Double> dist)
    {
        double min_dist = dist.get(0);
        int min_index = 0;

        for (int i =1; i<dist.size(); i++)
        {
            if (dist.get(i)<min_dist)
            {
                min_dist = dist.get(i);
                min_index = i;
            }

        }

        return min_index;
    }

    //Get calibration name from the calibration's position in the 'Calibrations.csv' file
    public String getCurve(int n)
    {

        int count=0;
        String line="";
        String name="";

        String fileName = "Calibrations.csv";

        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), fileName);
        try {

            BufferedReader inputReader = new BufferedReader(new FileReader(file));
            while ((line = inputReader.readLine()) != null) {
                count++;
                if (count == 1)
                {
                    line = line.replace("\"", "");
                    name = line.split(",")[0];
                }
                if (count != n)
                    continue;

                line = line.replace("\"", "");
                name = line.split(",")[0];
                break;
            }
        }

        catch (IOException e){
        e.printStackTrace();
        }

        return name;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_get_value, menu);
        return true;
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
